package se.experis.noticeboardtask.util;

import se.experis.noticeboardtask.model.UserData;

import java.util.Date;
import java.util.HashMap;

public class SessionKeeper {
    private static SessionKeeper sessionKeeperInstance = null;

    private HashMap<String, UserData> validSessions = new HashMap<>();

    public UserData CheckSession(String session) {
        return validSessions.get(session);
    }

    public void AddSession(String session,UserData user){
        user.date = new Date();
        validSessions.put(session, user);
    }

    public void RemoveSession(String session){
        if(validSessions.containsKey(session)) {
            validSessions.remove(session);
        }
    }

    // Singleton
    public static SessionKeeper getInstance(){
        if (sessionKeeperInstance == null){
            sessionKeeperInstance = new SessionKeeper();
        }

        return sessionKeeperInstance;
    }

}

