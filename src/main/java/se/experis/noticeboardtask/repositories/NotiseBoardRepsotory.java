package se.experis.noticeboardtask.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import se.experis.noticeboardtask.model.NoticeBoard;

public interface NotiseBoardRepsotory extends JpaRepository<NoticeBoard, Integer> {
    NoticeBoard getById(Integer id);
}
