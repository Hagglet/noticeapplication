package se.experis.noticeboardtask.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import se.experis.noticeboardtask.model.NoticeBoard;
import se.experis.noticeboardtask.model.Reply;
import se.experis.noticeboardtask.model.UserData;
import se.experis.noticeboardtask.repositories.NotiseBoardRepsotory;
import se.experis.noticeboardtask.util.SessionKeeper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

@Controller
public class NoticeBoardController {

    @Autowired
    private NotiseBoardRepsotory notiseBoardRepsotory;

    @GetMapping(value = "/login")
    public String login(HttpServletRequest request, HttpSession session, Model model) {
        model.addAttribute("sessionId", session.getId());
        model.addAttribute("loginStatus", SessionKeeper.getInstance().CheckSession(session.getId()));
        model.addAttribute("UserLogin", new UserData());
        return "login";
    }

    @GetMapping("/listnotice")
    public String getAll(Model model) {
        List<NoticeBoard> allnotice = notiseBoardRepsotory.findAll();
        model.addAttribute("allNotice", allnotice);
        return "listnotice";
    }

    @GetMapping("/listwithreplies/{id}")
    public String getOne(@PathVariable Integer id, Model model) {
        NoticeBoard noticeBoard = notiseBoardRepsotory.getById(id);
        List<Reply> replies = noticeBoard.replies;
        model.addAttribute("Notice", noticeBoard);
        model.addAttribute("replies", replies);
        return "listwithreplies";
    }

    @GetMapping("/deletepost/{id}")
    public String deleteOne(@PathVariable Integer id, Model model,HttpSession session) {
        Optional<NoticeBoard> noticeToBeEdited = notiseBoardRepsotory.findById(id);
        NoticeBoard noticeBoard = noticeToBeEdited.get();
        if(noticeBoard.creator.equals(SessionKeeper.getInstance().CheckSession(session.getId()).username)) {
            notiseBoardRepsotory.deleteById(id);
            return "redirect:/listnotice";
        }
        return "redirect:/listnotice";
    }

    @GetMapping("/newpost")
    public String newPost(Model model) {
        System.out.println("new post");
        model.addAttribute("NoticeBoard", new NoticeBoard());
        return "newpost";
    }

    @GetMapping("/reply/{id}")
    public String newReply(@PathVariable Integer id, Model model) {
        NoticeBoard noticeBoard = notiseBoardRepsotory.getById(id);
        model.addAttribute("reply", new Reply());
        model.addAttribute("test", noticeBoard);
        return "reply";
    }

    @PostMapping("/redirect")
    public String redirect(NoticeBoard noticeBoard, HttpSession session) {
        UserData userData;
        if((userData = SessionKeeper.getInstance().CheckSession(session.getId())) != null) {
            noticeBoard.creator = SessionKeeper.getInstance().CheckSession(session.getId()).username;
            notiseBoardRepsotory.save(noticeBoard);
            return "redirect:/listnotice";
        }
        return "redirect:/login";
    }

    @PostMapping("/reply/addreply/{id}")
    public String addreply(@PathVariable Integer id, Reply reply) {
        NoticeBoard noticeBoard = notiseBoardRepsotory.getById(id);
        noticeBoard.replies.add(reply);
        notiseBoardRepsotory.save(noticeBoard);

        return "redirect:/listnotice";
    }

    @GetMapping("/editnotice/{id}")
    public String editNotice(@PathVariable Integer id, Model model, HttpSession session) {

        Optional<NoticeBoard> noticeToBeEdited = notiseBoardRepsotory.findById(id);
        NoticeBoard noticeBoard = noticeToBeEdited.get();
        if(noticeBoard.creator.equals(SessionKeeper.getInstance().CheckSession(session.getId()).username)) {
            model.addAttribute("editnotice", noticeBoard);
            return "editnotice";
        }

        return "redirect:/listnotice";
    }
    @GetMapping("/logout")
    public String logout(HttpSession session) {
        SessionKeeper.getInstance().RemoveSession(session.getId());
        return "redirect:/login";

    }

    @PatchMapping("/editnotice/update/{id}")
    public String updateNotice(@PathVariable Integer id, @RequestBody NoticeBoard noticeBoardNew) {
        Optional<NoticeBoard> noticeRepo = notiseBoardRepsotory.findById(id);
        NoticeBoard noticeboard = noticeRepo.get();

        if (noticeBoardNew.title != null) {
            System.out.println(noticeboard.title);
            noticeboard.title = noticeBoardNew.title;
        }
        if (noticeBoardNew.id != null) {
            noticeboard.id = noticeBoardNew.id;
        }
        if (noticeBoardNew.content != null) {
            noticeboard.content = noticeBoardNew.content;
        }
        if (noticeBoardNew.date != null) {
            noticeboard.date = noticeBoardNew.date;
        }
        if (noticeBoardNew.creator != null) {
            System.out.println(noticeboard.creator);
            noticeboard.creator = noticeBoardNew.creator;
        }

        notiseBoardRepsotory.save(noticeboard);
        return "redirect:/listnotice";
    }

    @PostMapping(value = "/saveSession")
    public String add(
            @RequestParam(value = "action", required = true)String action,
            @ModelAttribute("UserLogin") UserData loginData,
            HttpServletRequest request,
            HttpSession session,
            Model model) {
        if(action.equals("login")){
            if(loginData.username.equals("Kristoffer")) {
                if(loginData.password.equals("123")){
                    SessionKeeper.getInstance().AddSession(session.getId(),loginData);
                    return "redirect:/listnotice";
                }
            }
            else if (loginData.username.equals("Nisse")) {
                if(loginData.password.equals("123")){
                    SessionKeeper.getInstance().AddSession(session.getId(),loginData);
                    return "redirect:/listnotice";

                }
            }
            else if(loginData.username.equals("Kalle")) {
                if(loginData.password.equals("123")){
                    SessionKeeper.getInstance().AddSession(session.getId(),loginData);
                    return "redirect:/listnotice";
                }
            }
            else
            {
                return "login";
            }
        }
        return "redirect:/listnotice";
    }

}