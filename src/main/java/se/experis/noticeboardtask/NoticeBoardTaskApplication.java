package se.experis.noticeboardtask;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoticeBoardTaskApplication {

	public static void main(String[] args) {
		SpringApplication.run(NoticeBoardTaskApplication.class, args);
	}

}
